@extends('layouts.app')

@section('content')


    <div class="panel-body">

    @include('common.errors')

        <form action="{{url('users/' . $user->id)}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <h2>User</h2>
            <div class="form-group">
                <label for="user-first-name" class="col-sm-3 control-label">First Name</label>

                <div class="col-sm-6">
                    <input type="text" name="first_name" id="user-first-name" class="form-control" value="{{ $user->first_name }}">
                </div>
            </div>

            <div class="form-group">
                <label for="user-last-name" class="col-sm-3 control-label">First Name</label>

                <div class="col-sm-6">
                    <input type="text" name="last_name" id="user-last-name" class="form-control" value="{{ $user->last_name }}">
                </div>
            </div>

            <div class="form-group">
                <label for="user-email" class="col-sm-3 control-label">Email</label>

                <div class="col-sm-6">
                    <input type="text" name="email" id="user-email" class="form-control"
                           value="{{ $user->email }}">
                </div>
            </div>

            <!-- Replace Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Edit User
                    </button>
                    <a class="btn btn-danger" href="{{ url()->previous() }}" role="button">Cancel</a>
                </div>
            </div>
        </form>
    </div>

@endsection