@extends('layouts.app')

@section('script')
    $.ajaxSetup({

    headers: {

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

    });
    $(document).ready(function () {
    $('.state-item').on('change', function (e) {
    var id = $(this).parent().find("input").val();
    var state = $(this).find("option:selected").val();

    $.ajax({

    type: 'GET',

    url: '/task/' + id + '/change-state',

    data: {state: state},

    success: function (data) {

    alert(data.success);

    }

    });
    });



    var exportFile = function (type) {
    var url = "/tasks/export-" + type;
    if ($('#users').length > 0) {
    url += "/"+ $('#users').val();
    }
    window.location.replace(url);
    };

    $('#button-csv').on('click', function () {
    exportFile('csv');
    return false;
    });

    $('#button-xml').on('click', function () {
    exportFile('xml');
    return false;

    });
    });

@endsection

@section('content')


    <div class="panel-body">
        @include('common.errors')

        <form action="{{url('task/')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            @if(Auth::user()->role->slug === 'admin')
            <h2>User</h2>
            <div class="form-group">
                <label for="users" class="col-sm-3 control-label">Choose
                    User</label>
                <div class="col-sm-6">
                    <select class="form-control" id="users" name="users" onchange="window.location.href='/tasks/'+this.value;">
                        @foreach ($users as $user)
                            @if (is_null(Request::route("user")))
                                @if($user->id === Auth::user()->id)
                                    <option value="{{$user->id}}"
                                            selected>{{$user->first_name. " ".$user->last_name}}</option>
                                @else
                                    <option value="{{$user->id}}">{{$user->first_name. " ".$user->last_name}}</option>
                                @endif
                            @else
                                @if ($user->id === Request::route("user")->id)
                                    <option value="{{$user->id}}"
                                            selected>{{$user->first_name. " ".$user->last_name}}</option>
                                @else
                                    <option value="{{$user->id}}">{{$user->first_name. " ".$user->last_name}}</option>
                                @endif
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            @endif

            <h2>Task</h2>
            <div class="form-group">
                <label for="task-name" class="col-sm-3 control-label">Name</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="task-name" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="task-description" class="col-sm-3 control-label">Description</label>

                <div class="col-sm-6">
                    <input type="text" name="description" id="task-description" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="task-state" class="col-sm-3 control-label">State</label>
                <div class="col-sm-6">
                    <select class="form-control" id="task-state" name="state">
                        <option value="new">New</option>
                        <option value="in-progress">In Progress</option>
                        <option value="finished">Finished</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div>
        </form>
    </div>

    @if (count($tasks) > 0)
        <div class="panel panel-default">
            <h2 class="panel-heading">
                Current Tasks
                <a class="btn btn-secondary" id="button-csv" href=""
                   role="button">CSV</a>
                <a class="btn btn-secondary" id="button-xml" href=""
                   role="button">XML</a>
            </h2>


            <div class="panel-body">
                <table class="table table-striped task-table">

                    <thead>
                    <th>Name</th>
                    <th>Description</th>
                    <th>State</th>
                    <th>&nbsp;</th>
                    </thead>

                    <tbody>
                    @foreach ($tasks as $task)
                        <tr>
                            <td class="table-text">
                                <div>{{ $task->name }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $task->description }}</div>
                            </td>
                            <td class="table-text">
                                <input type="hidden" value="{{$task->id}}">
                                <select class="form-control state-item">
                                    <option {{$task->state == "new" ? 'selected':''}} value="new">
                                        New
                                    </option>
                                    <option {{$task->state == "in-progress" ? 'selected':''}} value="in-progress">In
                                        Progress
                                    </option>
                                    <option {{$task->state == "finished" ? 'selected':''}} value="finished">Finished
                                    </option>
                                </select>
                            </td>

                            <td>
                                <form action="{{url('task/' . $task->id)}}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button type="submit" id="delete-task-{{ $task->id }}" class="btn btn-danger">
                                        <i class="fa fa-btn fa-trash"></i>Delete
                                    </button>

                                    <a class="btn btn-secondary" href="{{url('task/' . $task->id.'/edit')}}"
                                       role="button">Edit</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection