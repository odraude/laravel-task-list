@extends('layouts.app')

@section('content')


    <div class="panel-body">

    @include('common.errors')

        <form action="{{url('task/' . $task->id)}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <h2>Task</h2>
            <div class="form-group">
                <label for="task-name" class="col-sm-3 control-label">Name</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="task-name" class="form-control" value="{{ $task->name }}">
                </div>
            </div>

            <div class="form-group">
                <label for="task-description" class="col-sm-3 control-label">Description</label>

                <div class="col-sm-6">
                    <input type="text" name="description" id="task-description" class="form-control"
                           value="{{ $task->description }}">
                </div>
            </div>

            <div class="form-group">
                <label for="task-state" class="col-sm-3 control-label">State</label>
                <div class="col-sm-6">
                    <select class="form-control" id="task-state" name="state">
                        <option {{$task->state == "new" ? 'selected':''}} value="new">
                            New
                        </option>
                        <option {{$task->state == "in-progress" ? 'selected':''}} value="in-progress">In Progress
                        </option>
                        <option {{$task->state == "finished" ? 'selected':''}} value="finished">Finished
                        </option>
                    </select>
                </div>
            </div>

            <!-- Edit Task Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Edit Task
                    </button>
                    <a class="btn btn-danger" href="{{ url()->previous() }}" role="button">Cancel</a>
                </div>
            </div>
        </form>
    </div>

@endsection