<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Task;
use App\Repositories\TaskRepository;

class TaskController extends Controller
{
    protected $tasks;
    protected $user;

    public function __construct(TaskRepository $tasks)
    {
        $this->middleware('auth');
        $this->tasks = $tasks;
    }

    public function index(Request $request, User $user = null)
    {
        $this->user = \Auth::user();
        if ($this->user->role->slug !== 'admin') {
            return view('tasks.index', [
                'tasks' => $this->tasks->forUser($request->user()),
            ]);
        }

        if (is_null($user)) {
            return view('tasks.index', [
                'tasks' => $this->tasks->forUser($request->user()),
                'users' => User::all(),
            ]);
        }

        return view('tasks.index', [
            'tasks' => $this->tasks->forUser($user),
            'users' => User::all(),
        ]);

    }

    public function edit(Task $task)
    {
        return view('tasks.edit', [
            'task' => $task,
        ]);
    }

    public function store(Request $request)
    {

        $this->user = \Auth::user();
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'max:255',
            'state' => 'in:new,in-progress,finished',
        ]);

        $task = [
            'name' => $request->name,
            'description' => $request->description,
            'state' => $request->state,
        ];

        if ($this->user->role->slug !== 'admin') {
            $request->user()->tasks()->create($task);
            return redirect('/tasks');
        }

        if (is_null($request->users) || $request->user()->id == $request->users) {
            $request->user()->tasks()->create($task);
            return redirect('/tasks');
        }

        $user = User::find($request->users);
        $user->tasks()->create($task);
        return redirect('/tasks/' . $user->id);

    }

    public function destroy(Request $request, Task $task)
    {

        $this->user = \Auth::user();
        $user_id = $task->user->id;
        if ($this->user->role->slug !== 'admin') {
            $this->authorize('isOwner', $task);
            $task->delete();
            return redirect('/tasks');
        }

        $task->delete();
        return redirect('/tasks/' . $user_id);

    }

    public function update(Request $request, Task $task)
    {
        $this->user = \Auth::user();
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'max:255',
            'state' => 'in:new,in-progress,finished',
        ]);

        $task_data = [
            'name' => $request->name,
            'description' => $request->description,
            'state' => $request->state,
        ];

        if ($this->user->role->slug !== 'admin') {
            $this->authorize('isOwner', $task);
            $task->update($task_data);
            return redirect('/tasks');
        }

        $task->update($task_data);

        return redirect('/tasks/' . $task->user->id);


    }

    public function changeState(Request $request, Task $task)
    {
        $this->user = \Auth::user();
        $this->validate($request, [
            'state' => 'in:new,in-progress,finished',
        ]);
        $state_data = [
            'state' => $request->state,
        ];
        if ($this->user->role->slug !== 'admin') {
            $this->authorize('isOwner', $task);
        }

        $task->update($state_data);
        return response()->json(['success' => 'Data is successfully updated']);
    }

    public function exportCsv(Request $request, User $user = null)
    {
        $this->user = \Auth::user();
        $csvExporter = new \Laracsv\Export();
        $columns = ['name', 'description', 'state'];
        if ($this->user->role->slug !== 'admin') {
            $csvExporter->build($this->tasks->forUser($request->user()), $columns)->download();
            return;
        }

        $csvExporter->build($this->tasks->forUser($user), $columns)->download();

    }

    public function exportXml(Request $request, User $user = null)
    {

        $this->user = \Auth::user();
        if ($this->user->role->slug !== 'admin' || is_null($user) ) {
            $task_array = $this->tasks->forUser($request->user())->toArray();
        }else{
            $task_array = $this->tasks->forUser($user)->toArray();
        }

        foreach ($task_array as $key => $value) {
            $task_array["task_" . $key] = $value;
            unset($task_array[$key]);
        }

        $xml = \Array2XML::createXML('root_node_name', $task_array);

        return response($xml->saveXML())
            ->withHeaders([
                'Content-Type' => 'text/xml'
            ]);


    }
}
