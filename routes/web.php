<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/users', 'Auth\UsersController@index')->middleware('role:admin')->name('users');
Route::get('/users/{user}/edit', 'Auth\UsersController@edit')->middleware('role:admin');
Route::put('/users/{user}', 'Auth\UsersController@update')->middleware('role:admin');
Route::delete('/users/{user}', 'Auth\UsersController@destroy')->middleware('role:admin');

Route::get('/tasks/export-csv/{user?}', 'TaskController@exportCsv');
Route::get('/tasks/export-xml/{user?}', 'TaskController@exportXml');
Route::get('/tasks/{user?}', 'TaskController@index')->name('tasks');
Route::post('/task/{user?}', 'TaskController@store');
Route::delete('/task/{task}', 'TaskController@destroy');
Route::put('/task/{task}', 'TaskController@update');
Route::get('/task/{task}/edit', 'TaskController@edit');
Route::get('/task/{task}/change-state','TaskController@changeState');



