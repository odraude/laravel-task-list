<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //

    protected $fillable = ['name','description','state'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
