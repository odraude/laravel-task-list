<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_regular = Role::where('slug', 'regular')->first();
        $role_admin = Role::where('slug', 'admin')->first();

        $user = new User();
        $user->first_name = 'First';
        $user->last_name = 'Last';
        $user->email = 'firstlast@example.com';
        $user->password = bcrypt('secret');
        $user->role()->associate($role_regular);
        $user->save();

        $user = new User();
        $user->first_name = 'User';
        $user->last_name = 'Administrator';
        $user->email = 'useradmin@example.com';
        $user->password = bcrypt('secret');
        $user->role()->associate($role_admin);
        $user->save();

    }
}
