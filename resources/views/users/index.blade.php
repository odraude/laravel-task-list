@extends('layouts.app')

@section('script')
@endsection

@section('content')

    @if (count($users) > 0)
        <div class="panel panel-default">
            <h2 class="panel-heading">
                Users List
            </h2>

            <div class="panel-body">
                <table class="table table-striped task-table">

                    <thead>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>&nbsp;</th>
                    </thead>

                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td class="table-text">
                                <div>{{ $user->first_name }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $user->last_name }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $user->email }}</div>
                            </td>

                            <td>
                                <form action="{{url('users/' . $user->id)}}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button type="submit" id="delete-task-{{ $user->id }}" class="btn btn-danger">
                                        <i class="fa fa-btn fa-trash"></i>Delete
                                    </button>

                                    <a class="btn btn-secondary" href="{{url('users/' . $user->id.'/edit')}}"
                                       role="button">Edit</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection